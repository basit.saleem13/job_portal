import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutter/foundation.dart' show kIsWeb;

class HomeItem extends StatelessWidget {
  const HomeItem(
      {super.key,
      required this.title,
      required this.desc1,
      required this.desc2,
      required this.desc3});

  final String title, desc1, desc2, desc3;

  @override
  Widget build(BuildContext context) {
    return Scrollbar(
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            const SizedBox(
              height: 25,
            ),
            Text(
              title,
              textAlign: TextAlign.center,
              style: const TextStyle(
                  color: Color(0xFF4A5568),
                  fontSize: 21,
                  fontWeight: FontWeight.w600),
            ),
            const SizedBox(
              height: 10,
            ),
            kIsWeb
                ? Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Text(
                        '1.',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: Color(0xFF4A5568),
                            fontSize: 100,
                            fontWeight: FontWeight.w600),
                      ),
                      Text(
                        desc1,
                        textAlign: TextAlign.center,
                        style: const TextStyle(
                          color: Color(0xFF4A5568),
                          fontSize: 15,
                        ),
                      ),
                      const SizedBox(
                        width: 30,
                      ),
                      SvgPicture.asset(
                          'assets/images/undraw_Profile_data_re_v81r.svg'),
                    ],
                  )
                : Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      const Text(
                        '1.',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: Color(0xFF4A5568),
                            fontSize: 100,
                            fontWeight: FontWeight.w600),
                      ),
                      Flexible(
                        child: Column(
                          children: [
                            SvgPicture.asset(
                                'assets/images/undraw_Profile_data_re_v81r.svg'),
                            const SizedBox(
                              height: 15,
                            ),
                            Text(
                              desc1,
                              textAlign: TextAlign.center,
                              style: const TextStyle(
                                color: Color(0xFF4A5568),
                                fontSize: 15,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
            const SizedBox(
              height: kIsWeb ? 80 : 10,
            ),
            kIsWeb
                ? Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SvgPicture.asset('assets/images/undraw_task_31wc.svg'),
                      const SizedBox(
                        width: 100,
                      ),
                      const Text(
                        '2.',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: Color(0xFF4A5568),
                            fontSize: 100,
                            fontWeight: FontWeight.w600),
                      ),
                      Text(
                        desc2,
                        textAlign: TextAlign.center,
                        style: const TextStyle(
                          color: Color(0xFF4A5568),
                          fontSize: 15,
                        ),
                      )
                    ],
                  )
                : Column(
                    children: [
                      Row(
                        children: <Widget>[
                          const Text(
                            '2.',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: Color(0xFF4A5568),
                                fontSize: 100,
                                fontWeight: FontWeight.w600),
                          ),
                          Text(
                            desc2,
                            textAlign: TextAlign.center,
                            style: const TextStyle(
                              color: Color(0xFF4A5568),
                              fontSize: 15,
                            ),
                          ),
                        ],
                      ),
                      SvgPicture.asset('assets/images/undraw_task_31wc.svg'),
                    ],
                  ),
            const SizedBox(
              height: kIsWeb ? 80 : 15,
            ),
            kIsWeb
                ? Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Text(
                        '3.',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: Color(0xFF4A5568),
                            fontSize: 100,
                            fontWeight: FontWeight.w600),
                      ),
                      Flexible(
                        child: Text(
                          desc3,
                          textAlign: TextAlign.center,
                          maxLines: 2,
                          style: const TextStyle(
                            color: Color(0xFF4A5568),
                            fontSize: 15,
                          ),
                        ),
                      ),
                      const SizedBox(
                        width: 100,
                      ),
                      SvgPicture.asset(
                          'assets/images/undraw_personal_file_222m.svg'),
                    ],
                  )
                : Column(
                    children: [
                      Row(
                        children: <Widget>[
                          const Text(
                            '3.',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: Color(0xFF4A5568),
                                fontSize: 100,
                                fontWeight: FontWeight.w600),
                          ),
                          Flexible(
                            child: Text(
                              desc3,
                              textAlign: TextAlign.center,
                              maxLines: 2,
                              style: const TextStyle(
                                color: Color(0xFF4A5568),
                                fontSize: 15,
                              ),
                            ),
                          ),
                        ],
                      ),
                      SvgPicture.asset(
                          'assets/images/undraw_personal_file_222m.svg'),
                    ],
                  ),
          ],
        ),
      ),
    );
  }
}
