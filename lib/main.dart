import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:job_portal/home_item.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  static const MaterialColor white = MaterialColor(
    0xFFFFFFFF,
    <int, Color>{
      50: Color(0xFFFFFFFF),
      100: Color(0xFFFFFFFF),
      200: Color(0xFFFFFFFF),
      300: Color(0xFFFFFFFF),
      400: Color(0xFFFFFFFF),
      500: Color(0xFFFFFFFF),
      600: Color(0xFFFFFFFF),
      700: Color(0xFFFFFFFF),
      800: Color(0xFFFFFFFF),
      900: Color(0xFFFFFFFF),
    },
  );

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: white,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _indexSegmentSelected = 1;
  final PageController _pageController =
      PageController(initialPage: 1, viewportFraction: 0.8);
  final ScrollController scrollController = ScrollController();
  bool _isScrolled = false;

  @override
  void initState() {
    super.initState();
    _scrollListener();
  }

  void _scrollListener() {
    scrollController.addListener(() {
      if (scrollController.position.pixels >= 200) {
        WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
          if (mounted) {
            setState(() {
              _isScrolled = true;
            });
          }
        });
      } else {
        WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
          if (mounted) {
            setState(() {
              _isScrolled = false;
            });
          }
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: NestedScrollView(
          controller: scrollController,
          floatHeaderSlivers: true,
          headerSliverBuilder: (context, innerBoxIsScrolled) {
            return [
              SliverAppBar(
                title: const Text(''),
                pinned: true,
                elevation: 5,
                shape: const ContinuousRectangleBorder(
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(30),
                        bottomRight: Radius.circular(30))),
                actions: <Widget>[
                  _isScrolled
                      ? Row(
                          children: <Widget>[
                            const Text(
                              'Jetzt Klicken',
                              style: TextStyle(color: Color(0xFF4A5568)),
                            ),
                            const SizedBox(
                              width: 10,
                            ),
                            OutlinedButton(
                              onPressed: () {},
                              child: const Text(
                                'Kostenlos Registrieren',
                                style: TextStyle(color: Color(0xFF319795)),
                              ),
                            )
                          ],
                        )
                      : const SizedBox.shrink(),
                  MaterialButton(
                    onPressed: () {},
                    textColor: const Color(0xFF81E6D9),
                    child: const Text('Login'),
                  ),
                ],
              ),
              SliverAppBar(
                floating: false,
                expandedHeight: 200,
                pinned: false,
                forceElevated: innerBoxIsScrolled,
                backgroundColor: const Color(0xFFE6FFFA),
                flexibleSpace: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      SingleChildScrollView(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            const Text(
                              'Define job',
                              style: TextStyle(
                                  fontSize: 25, fontWeight: FontWeight.w800),
                            ),
                            const Text(
                              'website',
                              style: TextStyle(
                                  fontSize: 25, fontWeight: FontWeight.w800),
                            ),
                            const SizedBox(
                              height: 20,
                            ),
                            Container(
                              height: 44,
                              decoration: const BoxDecoration(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(12)),
                                  gradient: LinearGradient(colors: [
                                    Color(0xFF319795),
                                    Color(0xFF3182CE)
                                  ])),
                              child: MaterialButton(
                                onPressed: () {},
                                child: const Text(
                                  'Kostenlos Registrieren',
                                  style: TextStyle(
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      Flexible(
                          child: SvgPicture.asset(
                              'assets/images/undraw_agreement_aajr.svg'))
                    ],
                  ),
                ),
              ),
            ];
          }, // The content of the scroll view
          body: Column(
            children: [
              Container(
                  padding: const EdgeInsets.all(16),
                  child: buildSegmentedControl()),
              Expanded(
                  child: PageView(
                controller: _pageController,
                onPageChanged: (index) {
                  changePage(index, animate: false);
                },
                children: [
                  buildPage(
                      title: 'Drei einfache Schritte zu deinem neuen Job',
                      desc1: 'Erstellen dein Lebenslauf',
                      desc2: 'Erstellen dein Lebenslauf',
                      desc3: 'Mit nur einem Klick bewerben',
                      index: 0),
                  buildPage(
                      title:
                          'Drei einfache Schritte zu deinem neuen Mitarbeiter',
                      desc1: 'Erstellen dein Lebenslauf',
                      desc2: 'Erstellen dein Lebenslauf',
                      desc3: 'Mit nur einem Klick bewerben',
                      index: 1),
                  buildPage(
                      title: 'Drei einfache Schritte zu deinem neuen Job',
                      desc1: 'Erstellen dein Lebenslauf',
                      desc2: 'Erstellen dein Lebenslauf',
                      desc3: 'Mit nur einem Klick bewerben',
                      index: 2),
                ],
              ))
            ],
          )),
    );
  }

  Widget buildPage(
      {required String title,
      required String desc1,
      required String desc2,
      required String desc3,
      int index = 0}) {
    return AnimatedPadding(
      duration: const Duration(milliseconds: 500),
      padding: EdgeInsets.all(_indexSegmentSelected == index ? 0 : 28),
      child: AnimatedOpacity(
          duration: const Duration(milliseconds: 500),
          opacity: _indexSegmentSelected == index ? 1 : 0.3,
          child: HomeItem(
            title: title,
            desc1: desc1,
            desc2: desc2,
            desc3: desc3,
          )),
    );
  }

  Widget buildSegmentedControl() {
    return Container(
        decoration: BoxDecoration(
            border: Border.all(color: Colors.grey.shade400, width: 1),
            color: Colors.white,
            borderRadius: BorderRadius.circular(16)),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            buildSegmentItem("Arbeitnehmer", index: 0),
            buildSegmentItem("Arbeitgeber", index: 1),
            buildSegmentItem("Temporärbüro", index: 2)
          ],
        ));
  }

  Widget buildSegmentItem(String title, {int index = 0}) {
    bool selected = _indexSegmentSelected == index;
    return GestureDetector(
      onTap: () {
        changePage(index);
      },
      child: Container(
        decoration: selected
            ? BoxDecoration(
                color: const Color(0xFF81E6D9),
                borderRadius: index == 0
                    ? const BorderRadius.only(
                        bottomLeft: Radius.circular(12),
                        topLeft: Radius.circular(12),
                      )
                    : index == 1
                        ? const BorderRadius.all(Radius.circular(12))
                        : const BorderRadius.only(
                            bottomRight: Radius.circular(12),
                            topRight: Radius.circular(12),
                          ),
              )
            : null,
        padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 12),
        child: Text(title,
            style: TextStyle(
                fontSize: 14,
                color: selected ? Colors.white : const Color(0xFF319795))),
      ),
    );
  }

  changePage(int index, {bool animate = true}) async {
    if (animate) {
      await _pageController.animateToPage(index,
          duration: const Duration(milliseconds: 300),
          curve: Curves.decelerate);
    }
    setState(() {
      _indexSegmentSelected = index;
    });
  }
}
